﻿using RPGHeroes.CustomErrors;
using RPGHeroes.Heroes;
using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTesting.heroTests
{
    public class HeroTests
    {
        [Fact]
        public void EquipWeapon_InputValidWeaponInWeaponSlot_ShouldAddWeaponToEquipments()
        {
            Hero hero = new Warrior("Thor");
            Weapon weapon = new Weapon("Ashbringer", 1, Slot.WEAPON, WeaponType.SWORD, 30);

            Item expected = weapon;

            hero.EquipWeapon(Slot.WEAPON, weapon);

            Weapon actual = (Weapon)hero.Equipment.GetValueOrDefault(Slot.WEAPON);

            Assert.Equal(expected, weapon);
            
        }

        [Fact]
        public void EquipWeapon_WeaponWithGreaterRequiredLevelThanHero_ShouldThrowError()
        {
            Hero hero = new Warrior("Thor");
            Weapon weapon = new Weapon("Ashbringer", 20, Slot.WEAPON, WeaponType.SWORD, 30);            

            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(Slot.WEAPON, weapon));

        }

        [Fact]
        public void EquipWeapon_WeaponWithWeaponTypeNotEquipableByWarrior_ShouldThrowError()
        {
            Hero hero = new Warrior("Thor");
            Weapon weapon = new Weapon("Ashbringer", 1, Slot.WEAPON, WeaponType.STAFF, 30);

            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(Slot.WEAPON, weapon));
        }


        //ArmorTests

        [Fact]
        public void EquipArmor_ValidArmorInArmorSlot_ShouldAddArmorToEquipments()
        {
            Hero hero = new Rogue("Loke");
            Armor armor = new Armor(ArmorType.LEATHER, new HeroAttribute(3, 2, 1), "Leather Body", 1, Slot.BODY);

            Armor expected = armor;

            hero.EquipArmor(Slot.BODY, armor);

            Armor actual = (Armor)hero.Equipment.GetValueOrDefault(Slot.BODY);

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void EquipArmor_ArmorWithGreaterRequiredLevelThanHero_ShouldThrowInvalidArmorError()
        {
            Hero hero = new Rogue("Loke");
            Armor armor = new Armor(ArmorType.LEATHER, new HeroAttribute(3, 2, 1), "Leather Body", 40, Slot.BODY);

            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(Slot.BODY, armor));

        }

        [Fact]
        public void EquipArmor_ArmorWithArmorTypeNotEquipableByWarrior_ShouldThrowInvalidArmorError()
        {
            Hero hero = new Warrior("Thor");
            Armor armor = new Armor(ArmorType.LEATHER, new HeroAttribute(3, 2, 1), "Leather Body", 1, Slot.BODY);

            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(Slot.BODY, armor));
        }


        [Fact]
        public void CalculateTotalAttributes_LevelFiveWarriorWithNoEquipment_ShouldHaveCorrectAttributes()
        {
            Warrior hero = new("Thor");
            int level = 5;
            HeroAttribute initialWarrior = new HeroAttribute(5, 2, 1);
            HeroAttribute eachLevelup = new HeroAttribute(3, 2, 1);

            HeroAttribute expected = new HeroAttribute(initialWarrior.Strenght+(eachLevelup.Strenght*level), initialWarrior.Dexterity+ (eachLevelup.Dexterity * level), initialWarrior.Intelligence+ (eachLevelup.Intelligence * level));
            
            for(int i=0; i<level; i++)
            {
                hero.LevelUp();
            }

            HeroAttribute actual = hero.CalculateTotalAttributes();

            Assert.True(expected.AttributeEquals(actual));
        }

        [Fact]
        public void CalculateTotalAttributes_LevelSixWarriorWithChestArmor_ShouldHaveCorrectAttributes()
        {
            Warrior hero = new("Thor");
            int level = 6;
            HeroAttribute initialWarrior = new HeroAttribute(5, 2, 1);
            HeroAttribute eachLevelup = new HeroAttribute(3, 2, 1);
            HeroAttribute chestAttribute = new(5, 2, 1);
            Armor chest = new Armor(ArmorType.PLATE, chestAttribute, "Titanium Plate", 3, Slot.BODY);
            HeroAttribute expected = new HeroAttribute(initialWarrior.Strenght + (eachLevelup.Strenght * level)+chestAttribute.Strenght, 
                                                        initialWarrior.Dexterity + (eachLevelup.Dexterity * level) +chestAttribute.Dexterity, 
                                                        initialWarrior.Intelligence + (eachLevelup.Intelligence * level)+chestAttribute.Intelligence);

            for (int i = 0; i < level; i++)
            {
                hero.LevelUp();
            }

            hero.EquipArmor(Slot.BODY, chest);

            HeroAttribute actual = hero.CalculateTotalAttributes();

            Assert.True(expected.AttributeEquals(actual));
        }

        [Fact]
        public void CalculateTotalAttributes_LevelSixWarriorWithTwoEquipments_ShouldHaveCorrectAttributes()
        {
            Warrior hero = new("Thor");
            int level = 6;
            HeroAttribute initialWarrior = new HeroAttribute(5, 2, 1);
            HeroAttribute eachLevelup = new HeroAttribute(3, 2, 1);
            HeroAttribute chestAttribute = new(5, 2, 1);
            HeroAttribute helmetAttribute = new(3, 1, 1);
            Armor helmet = new Armor(ArmorType.PLATE, helmetAttribute, "Plate helmet", 2, Slot.HEAD);
            Armor chest = new Armor(ArmorType.PLATE, chestAttribute, "Titanium Plate", 3, Slot.BODY);
            HeroAttribute expected = new HeroAttribute(initialWarrior.Strenght + (eachLevelup.Strenght * level) + chestAttribute.Strenght + helmetAttribute.Strenght,
                                                        initialWarrior.Dexterity + (eachLevelup.Dexterity * level) + chestAttribute.Dexterity + helmetAttribute.Dexterity,
                                                        initialWarrior.Intelligence + (eachLevelup.Intelligence * level) + chestAttribute.Intelligence + helmetAttribute.Intelligence);

            for (int i = 0; i < level; i++)
            {
                hero.LevelUp();
            }

            hero.EquipArmor(Slot.BODY, chest);
            hero.EquipArmor(Slot.HEAD, helmet);

            HeroAttribute actual = hero.CalculateTotalAttributes();

            Assert.True(expected.AttributeEquals(actual));
        }

        [Fact]
        public void CalculateTotalAttributes_WarriorWithTwoEquipmentsReplaceOne_ShouldHaveCorrectAttributes()
        {
            Warrior hero = new("Thor");
            int level = 6;
            HeroAttribute initialWarrior = new HeroAttribute(5, 2, 1);
            HeroAttribute eachLevelup = new HeroAttribute(3, 2, 1);
            HeroAttribute chestAttribute = new(5, 2, 1);
            HeroAttribute bodyAttribute = new(1, 0, 0);
            HeroAttribute helmetAttribute = new(3, 1, 1);
            Armor helmet = new Armor(ArmorType.PLATE, helmetAttribute, "Plate helmet", 2, Slot.HEAD);
            Armor rustyBody = new Armor(ArmorType.PLATE, bodyAttribute, "Rusty armor", 1, Slot.BODY);
            Armor chest = new Armor(ArmorType.PLATE, chestAttribute, "Titanium Plate", 3, Slot.BODY);

            HeroAttribute expected = new HeroAttribute(initialWarrior.Strenght + (eachLevelup.Strenght * level) + chestAttribute.Strenght + helmetAttribute.Strenght,
                                                        initialWarrior.Dexterity + (eachLevelup.Dexterity * level) + chestAttribute.Dexterity + helmetAttribute.Dexterity,
                                                        initialWarrior.Intelligence + (eachLevelup.Intelligence * level) + chestAttribute.Intelligence + helmetAttribute.Intelligence);

            for (int i = 0; i < level; i++)
            {
                hero.LevelUp();
            }

            hero.EquipArmor(Slot.BODY, rustyBody);
            hero.EquipArmor(Slot.HEAD, helmet);
            hero.EquipArmor(Slot.BODY, chest);

            HeroAttribute actual = hero.CalculateTotalAttributes();

            Assert.True(expected.AttributeEquals(actual));
            
        }
        [Fact]
        public void CalculateDamage_WarriorWithNoEquipment_ShouldReturnCorrectDamage()
        {
            Hero hero = new Warrior("Thor");

            HeroAttribute initialAttributes = new(5, 2, 1);
            int expected = (1 + initialAttributes.Strenght / 100);

            int actual = hero.CalculateDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WarriorWithWeapon_ShouldReturnCorrectDamage()
        {
            Hero hero = new Warrior("Thor");
         
            HeroAttribute initialAttributes = new(5, 2, 1);
            int weaponDamage = 30;
            Weapon weapon = new Weapon("Ashbringer", 1, Slot.WEAPON, WeaponType.SWORD, weaponDamage);
            int expected =  weaponDamage * (1 + initialAttributes.Strenght / 100);

            hero.EquipWeapon(Slot.WEAPON, weapon);

            int actual = hero.CalculateDamage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_WarriorWithWeaponAndArmor_ShouldReturnCorrectDamage()
        {
            Hero hero = new Warrior("Thor");

            HeroAttribute initialAttributes = new(5, 2, 1);
            HeroAttribute armorAttribute = new(10, 2, 1);
            Armor armor = new(ArmorType.PLATE, armorAttribute, "Plate Helmet", 1, Slot.HEAD);

            int weaponDamage = 30;
            Weapon weapon = new Weapon("Ashbringer", 1, Slot.WEAPON, WeaponType.SWORD, weaponDamage);
            int expected = weaponDamage * (1 + (initialAttributes.Strenght+armorAttribute.Strenght) / 100);

            hero.EquipWeapon(Slot.WEAPON, weapon);
            hero.EquipArmor(Slot.HEAD, armor);

            int actual = hero.CalculateDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WarriorWithWeaponAndReplaceWeapon_ShouldReturnCorrectDamage()
        {
            Hero hero = new Warrior("Thor");

            HeroAttribute initialAttributes = new(5, 2, 1);

            int weaponDamage = 30;
            Weapon badWeapon = new("Rusty Hammer", 1, Slot.WEAPON, WeaponType.HAMMER, 10);
            Weapon weapon = new Weapon("Ashbringer", 1, Slot.WEAPON, WeaponType.SWORD, weaponDamage);
            int expected = weaponDamage * (1 + (initialAttributes.Strenght) / 100);

            hero.EquipWeapon(Slot.WEAPON, badWeapon);
            hero.EquipWeapon(Slot.WEAPON, weapon);


            int actual = hero.CalculateDamage();

            Assert.Equal(expected, actual);
        }
    }
}
