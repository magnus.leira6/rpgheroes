﻿using RPGHeroes.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTesting.heroTests
{
    public class RougeTests
    {
        //RogueTests
        [Fact]
        public void RogueWithName_ShouldHaveName()
        {
            string input = "Odin";
            Hero hero = new Rogue(input);
            string expected = input;

            Assert.Equal(expected, hero.Name);
        }

        [Fact]
        public void RogueWithName_ShouldHaveLevelOne()
        {
            string input = "Odin";
            Hero hero = new Rogue(input);
            int expected = 1;

            Assert.Equal(expected, hero.Level);
        }
        [Fact]
        public void RogueWithName_ShouldHaveLevelAttribute()
        {
            string input = "Odin";
            Hero hero = new Rogue(input);
            HeroAttribute attribute = new HeroAttribute(2, 6, 1);

            HeroAttribute expected = attribute;

            HeroAttribute actual = hero.LevelAttributes;

            Assert.True(expected.AttributeEquals(actual));
        }
        
        [Fact]
        public void LevelUp_SixTimes_ShouldBeLevelSeven()
        {
            Hero hero = new Rogue("Odin");
            int level = 7;
            for (int i = 1; i < level; i++)
            {
                hero.LevelUp();
            }

            int expected = level;

            int actual = hero.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_SixTimes_DexterityShouldBe30()
        {
            Hero hero = new Rogue("Odin");
            int level = 7;
            HeroAttribute attribute = new(2, 6, 1);
            HeroAttribute incrementAttribute = new(1, 4, 1);
            for (int i = 1; i < level; i++)
            {
                hero.LevelUp();
                attribute.Dexterity += incrementAttribute.Dexterity;
            }

            int expected = attribute.Dexterity;

            int actual = hero.LevelAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
    }
}
