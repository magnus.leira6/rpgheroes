﻿using RPGHeroes.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTesting.heroTests
{
    public class MageTests
    {
        [Fact]
        public void MageWithName_ShouldHaveName()
        {
            string input = "Odin";
            Hero hero = new Mage(input);
            string expected = input;

            Assert.Equal(expected, hero.Name);
        }

        [Fact]
        public void MageWithName_ShouldHaveLevelOne()
        {
            string input = "Odin";
            Hero hero = new Mage(input);
            int expected = 1;

            Assert.Equal(expected, hero.Level);
        }

        [Fact]
        public void MageWithName_ShouldHaveLevelAttribute()
        {
            string input = "Odin";
            Hero hero = new Mage(input);
            HeroAttribute attribute = new(1, 1, 8);

            HeroAttribute expected = attribute;

            HeroAttribute actual = hero.LevelAttributes;

            Assert.True(expected.AttributeEquals(actual));
        }
    }
}
