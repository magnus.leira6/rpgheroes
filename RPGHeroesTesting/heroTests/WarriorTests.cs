﻿using RPGHeroes.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTesting.heroTests
{
    public class WarriorTests
    {

        [Fact]
        public void WarriorWithName_ShouldHaveName()
        {
            string input = "Thor";
            Hero hero = new Warrior(input);
            string expected = input;

            Assert.Equal(expected, hero.Name);
        }
        [Fact]
        public void WarriorWithName_ShouldHaveLevelOne()
        {
            string input = "Thor";
            Hero hero = new Warrior(input);
            int expected = 1;

            Assert.Equal(expected, hero.Level);
        }

        [Fact]
        public void WarriorWithName_ShouldHaveAttributes()
        {
            string input = "Thor";
            Hero hero = new Warrior(input);
            HeroAttribute attributes = new HeroAttribute(5, 2, 1);
            HeroAttribute expected = attributes;

            HeroAttribute actual = hero.LevelAttributes;

            Assert.True(expected.AttributeEquals(actual));
        }
    }
}
