﻿using RPGHeroes.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTesting.heroTests
{
    public class RangerTests
    {
        //RangerTests
        [Fact]
        public void RangerWithName_ShouldHaveName()
        {
            string input = "Odin";
            Hero hero = new Ranger(input);
            string expected = input;

            Assert.Equal(expected, hero.Name);
        }

        [Fact]
        public void RangerWithName_ShouldHaveLevelOne()
        {
            string input = "Odin";
            Hero hero = new Ranger(input);
            int expected = 1;

            Assert.Equal(expected, hero.Level);
        }
        [Fact]
        public void RangerWithName_ShouldHaveLevelAttributes()
        {
            string input = "Odin";
            Hero hero = new Ranger(input);
            HeroAttribute attribute = new HeroAttribute(1, 7, 1);

            HeroAttribute expected = attribute;

            HeroAttribute actual = hero.LevelAttributes;

            Assert.True(expected.AttributeEquals(actual));
        }
    }
}
