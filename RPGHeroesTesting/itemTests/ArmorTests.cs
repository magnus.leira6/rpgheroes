﻿using RPGHeroes.Heroes;
using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTesting.itemTests
{
    public class ArmorTests
    {
        [Fact]
        public void CreateAmorWithName_ShouldHaveName()
        {
            string input = "Platebody";
            Item armor = new Armor(ArmorType.PLATE, new HeroAttribute(3, 2, 1), input, 40, Slot.BODY);

            string expected = input;

            string actual = armor.Name;

            Assert.Equal(expected, actual);

        }
        [Fact]
        public void CreateAmorWithRequiredLevel_ShouldHaveRequiredLevel()
        {
            int input = 40;
            Item armor = new Armor(ArmorType.PLATE, new HeroAttribute(3, 2, 1), "Platebody", input, Slot.BODY);

            int expected = input;

            int actual = armor.RequiredLevel;

            Assert.Equal(expected, actual);

        }
        [Fact]
        public void CreateAmorWithBodySlot_ShouldHaveBodySlot()
        {
            Slot input = Slot.BODY;
            Item armor = new Armor(ArmorType.PLATE, new HeroAttribute(3, 2, 1), "Platebody", 40, input);

            Slot expected = input;

            Slot actual = armor.Slot;

            Assert.Equal(expected, actual);

        }
        [Fact]
        public void CreateAmorWithPlateArmorType_ShouldHavePlateArmorType()
        {
            ArmorType input = ArmorType.PLATE;
            Armor armor = new Armor(input, new HeroAttribute(3, 2, 1), "Platebody", 40, Slot.BODY);

            ArmorType expected = input;

            ArmorType actual = armor.ArmourType;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CreateAmorWithArmorAttributes_ShouldHaveArmorAttributes()
        {
            HeroAttribute input = new(4, 3, 1);
            Armor armor = new Armor(ArmorType.PLATE, input, "Platebody", 40, Slot.BODY);

            HeroAttribute expected = input;

            HeroAttribute actual = armor.ArmorAttribute;

            Assert.Equal(expected, actual);

        }



    }
}
