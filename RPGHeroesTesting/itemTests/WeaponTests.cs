﻿using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTesting.itemTests
{
    public class WeaponTests
    {

        [Fact]
        public void CreateWeaponWithName_ShouldHaveName()
        {
            string input = "Ashbringer";
            Item weapon = new Weapon(input, 70, Slot.WEAPON, WeaponType.SWORD, 20);

            string expected = input;

            string actual = weapon.Name;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CreateWeaponWithRequredLevel_ShouldHaveRequiredLevel()
        {
            int input = 70;
            Item weapon = new Weapon("Ashbringer", input, Slot.WEAPON, WeaponType.SWORD, 20);

            int expected = input;

            int actual = weapon.RequiredLevel;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CreateWeaponWithSlot_ShouldHaveWeaponSlot()
        {
            Slot input = Slot.WEAPON;
            Item weapon = new Weapon("Ashbringer", 70, input, WeaponType.SWORD, 20);

            Slot expected = input;

            Slot actual = weapon.Slot;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CreateWeaponWithWeaponType_ShouldHaveWeaponType()
        {
            WeaponType input = WeaponType.SWORD;
            Weapon weapon = new Weapon("Ashbringer", 70, Slot.WEAPON, input, 20);

            WeaponType expected = input;

            WeaponType actual = weapon.WeaponType;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CreateWeaponWithWeaponDamage_ShouldHaveWeaponDamage()
        {
            int input = 20;
            Weapon weapon = new Weapon("Ashbringer", 70, Slot.WEAPON, WeaponType.SWORD, input);

            int expected = input;

            int actual = weapon.WeaponDamage;

            Assert.Equal(expected, actual);

        }


    }
}
