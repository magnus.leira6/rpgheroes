﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Items
{
    public class Weapon: Item
    {
        public Weapon(string name, int requiredLevel, Slot slot, WeaponType weaponType, int weaponDamage) : base(name, requiredLevel, slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }

        public WeaponType WeaponType { get; set; }
        public int WeaponDamage { get; set; }

    }
}
