﻿using RPGHeroes.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Items
{
    public class Armor: Item
    {
        public ArmorType ArmourType { get; set; }
        public HeroAttribute ArmorAttribute { get; set; }

        public Armor(ArmorType armourType, HeroAttribute armorAttribute, string name, int requiredLevel, Slot slot): base(name, requiredLevel,slot)
        {
            ArmourType = armourType;
            ArmorAttribute = armorAttribute;
        }
    }
}
