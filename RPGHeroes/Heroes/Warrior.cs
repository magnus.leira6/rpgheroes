﻿using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
            ValidWeaponTypes.Add(WeaponType.HAMMER);
            ValidWeaponTypes.Add(WeaponType.AXE);
            ValidWeaponTypes.Add(WeaponType.SWORD);

            ValidArmorTypes.Add(ArmorType.PLATE);
            ValidArmorTypes.Add(ArmorType.MAIL);

            LevelAttributes = new HeroAttribute(5, 2, 1);
        }
        public override int CalculateDamage()
        {
            HeroAttribute totalAttributes = CalculateTotalAttributes();
            Weapon weapon = (Weapon)Equipment.Where(x => x.Key == Slot.WEAPON).Select(x => x.Value as Weapon).First();
            int damage;
            if(weapon != null)
            {
                damage = weapon.WeaponDamage * (1 + totalAttributes.Strenght / 100);
            }
            else
            {
                damage = 1 + totalAttributes.Strenght / 100;
            }
            

            return damage;
        }
        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.Intelligence += 1;
            LevelAttributes.Strenght += 3;
            LevelAttributes.Dexterity += 2;
        }

        public override string DisplayHero()
        {
            HeroAttribute total = CalculateTotalAttributes();
            int damage = CalculateDamage();
            StringBuilder stringBuilder = new();
            stringBuilder.AppendLine("Name: " + Name);
            stringBuilder.AppendLine("Class: Warrior");
            stringBuilder.AppendLine("Level: " + Level);
            stringBuilder.AppendLine("Total Strength: " + total.Strenght);
            stringBuilder.AppendLine("Total Dexterity: " + total.Dexterity);
            stringBuilder.AppendLine("Total Intelligence: " + total.Intelligence);
            stringBuilder.AppendLine("Damage: " + damage);

            return stringBuilder.ToString();
        }
    }
}
