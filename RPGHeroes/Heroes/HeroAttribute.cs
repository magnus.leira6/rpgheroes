﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public class HeroAttribute
    {
        public int Strenght { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute(int strenght, int dexterity, int intelligence)
        {
            Strenght = strenght;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public bool AttributeEquals(HeroAttribute heroAttribute)
        {
           return Strenght == heroAttribute.Strenght
                && Dexterity == heroAttribute.Dexterity
                && Intelligence == heroAttribute.Intelligence;
        }
    }
}
