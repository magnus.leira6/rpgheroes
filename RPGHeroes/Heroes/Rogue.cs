﻿using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name)
        {
            ValidWeaponTypes.Add(WeaponType.DAGGER);
            ValidWeaponTypes.Add(WeaponType.SWORD);

            ValidArmorTypes.Add(ArmorType.LEATHER);
            ValidArmorTypes.Add(ArmorType.MAIL);

            LevelAttributes = new HeroAttribute(2, 6, 1);
        }

        public override int CalculateDamage()
        {
            HeroAttribute totalAttributes = CalculateTotalAttributes();
            Weapon weapon = (Weapon)Equipment.Where(x => x.Key == Slot.WEAPON).Select(x => x.Value as Weapon).First();
            int damage;
            if (weapon != null)
            {
                damage = weapon.WeaponDamage * (1 + totalAttributes.Dexterity / 100);
            }
            else
            {
                damage = 1 + totalAttributes.Dexterity / 100;
            }


            return damage;
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.Intelligence += 1;
            LevelAttributes.Strenght += 1;
            LevelAttributes.Dexterity += 4;
        }

        public override string DisplayHero()
        {
            HeroAttribute total = CalculateTotalAttributes();
            int damage = CalculateDamage();
            StringBuilder stringBuilder = new();
            stringBuilder.AppendLine("Name: " + Name);
            stringBuilder.AppendLine("Class: Rogue");
            stringBuilder.AppendLine("Level: " + Level);
            stringBuilder.AppendLine("Total Strength: " + total.Strenght);
            stringBuilder.AppendLine("Total Dexterity: " + total.Dexterity);
            stringBuilder.AppendLine("Total Intelligence: " + total.Intelligence);
            stringBuilder.AppendLine("Damage: " + damage);

            return stringBuilder.ToString();
        }
    }
}
