﻿using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public class Mage : Hero
    {
        public Mage(string name) : base(name)
        {
            ValidWeaponTypes.Add(WeaponType.STAFF);
            ValidWeaponTypes.Add(WeaponType.WAND);

            ValidArmorTypes.Add(ArmorType.CLOTH);

            LevelAttributes = new(1, 1, 8);
        }

        public override int CalculateDamage()
        {
            HeroAttribute totalAttributes = CalculateTotalAttributes();
            Weapon weapon = (Weapon)Equipment.Where(x => x.Key == Slot.WEAPON).Select(x => x.Value as Weapon).First();
            int damage;
            if (weapon != null)
            {
                damage = weapon.WeaponDamage * (1 + totalAttributes.Intelligence / 100);
            }
            else
            {
                damage = 1 + totalAttributes.Intelligence / 100;
            }


            return damage;
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.Intelligence += 5;
            LevelAttributes.Strenght += 1;
            LevelAttributes.Dexterity += 1;
        }

        public override string DisplayHero()
        {
            HeroAttribute total = CalculateTotalAttributes();
            int damage = CalculateDamage();
            StringBuilder stringBuilder = new();
            stringBuilder.AppendLine("Name: " + Name);
            stringBuilder.AppendLine("Class: Mage");
            stringBuilder.AppendLine("Level: " + Level);
            stringBuilder.AppendLine("Total Strength: " + total.Strenght);
            stringBuilder.AppendLine("Total Dexterity: " + total.Dexterity);
            stringBuilder.AppendLine("Total Intelligence: " + total.Intelligence);
            stringBuilder.AppendLine("Damage: " + damage);

            return stringBuilder.ToString();
        }
    }
}
