﻿using RPGHeroes.CustomErrors;
using RPGHeroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public HeroAttribute LevelAttributes { get; set; }

        public Dictionary<Slot, Item> Equipment { get; set; }

        public List<WeaponType> ValidWeaponTypes { get; set; } = new List<WeaponType>();
        public List<ArmorType> ValidArmorTypes { get; set; } = new List<ArmorType>();

        protected Hero(string name)
        {
            Equipment = new Dictionary<Slot, Item>()
            {
                [Slot.WEAPON] = null,
                [Slot.BODY] = null,
                [Slot.LEGS] = null,
                [Slot.HEAD] = null,
            };
            Name = name;


        }

        public virtual void LevelUp()
        {
            Level++;
        }

        public void EquipArmor(Slot slot, Armor armor)
        {
            if(!ValidArmorTypes.Contains(armor.ArmourType))
            {
                throw new InvalidArmorException(GetType() + " can not equip " + armor.ArmourType);
            }

            if(Level < armor.RequiredLevel)
            {
                throw new InvalidArmorException("Hero level is to low to equip " + armor.Name);
            }

            Equipment[slot] = armor;
        }
        public void EquipWeapon(Slot slot, Weapon weapon)
        {
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException(GetType() + " can not equip " + weapon.WeaponType + "s");

            }

            if(Level < weapon.RequiredLevel)
            {
                throw new InvalidWeaponException("Hero level is too low to equip " + weapon.Name);
            }
            Equipment[slot] = weapon;
            
        }

        public abstract int CalculateDamage();

        public virtual HeroAttribute CalculateTotalAttributes()
        {
            List<Armor> equipentAttribute = (List<Armor>)Equipment.Where(x => x.Key != Slot.WEAPON && x.Value != null)
                .Select(y => y.Value).Select(a => a as Armor).ToList();

            if(equipentAttribute.Count > 0)
            {
                HeroAttribute heroAttributes = new HeroAttribute(0, 0, 0);
                heroAttributes.Strenght = LevelAttributes.Strenght + equipentAttribute.Sum(x => x.ArmorAttribute.Strenght);
                heroAttributes.Intelligence = LevelAttributes.Intelligence + equipentAttribute.Sum(x => x.ArmorAttribute.Intelligence);
                heroAttributes.Dexterity = LevelAttributes.Dexterity + equipentAttribute.Sum(x => x.ArmorAttribute.Dexterity);
                return heroAttributes;
            }
            else
            {
                return LevelAttributes;
            }

            

           
        }

        public abstract string DisplayHero();
       


    }
}
